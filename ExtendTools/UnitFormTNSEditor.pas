unit UnitFormTNSEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, IdStrings, Registry, ShellAPI;

type
  TFormTNSEditor = class(TForm)
    pnlSaveTNSName: TPanel;
    mmoTNS: TMemo;
    procedure pnlSaveTNSNameClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FOracleTNSName: string;
  public
    { Public declarations }
  end;

var
  PlugInID : Integer;
  FormTNSEditor: TFormTNSEditor;

  SYS_OracleHome: function: PChar; cdecl;
  IDE_SetStatusMessage: function(Text:PChar):bool;cdecl;
  IDE_GetAppHandle: function: Integer; cdecl; 
  IDE_GetEditorHandle: function: Integer; cdecl;
  IDE_GetText: function: PChar; cdecl;
  IDE_GetSelectedText: function: PChar; cdecl; 
  IDE_CreateWindow: procedure(WindowType: Integer; Text: PChar; Execute: Bool); cdecl;
  IDE_CreatePopupItem: procedure(ID, Index: Integer;Name, ObjectType: PChar); cdecl;
  IDE_SetReadOnly: procedure(ReadOnly: Bool); cdecl;
  IDE_GetReadOnly: function:Bool; cdecl;

implementation

{$R *.dfm}


{通用部分}
procedure ShowTips(Tips: string; IsErr: Boolean = False);
var
  TrueTips: string;
begin
  if IsErr then TrueTips := '【ERROR:】' + Tips
  else TrueTips := '【' + Tips + '】';
  if not IDE_SetStatusMessage(PChar(TrueTips)) then
    ShowMessage(TrueTips);
end;

{TNSEditor编辑部分}
procedure DoTNSEdit(FilePath: string);
var
  frmTNSEditor: TFormTNSEditor;
begin
  try
    Application.Handle := IDE_GetAppHandle;
    frmTNSEditor := TFormTNSEditor.Create(Application);
    frmTNSEditor.Position := poScreenCenter;
    frmTNSEditor.FOracleTNSName := FilePath;
    frmTNSEditor.ShowModal;
  finally
    if frmTNSEditor <> nil then
      FreeAndNil(frmTNSEditor);
  end;
end;

procedure TFormTNSEditor.FormShow(Sender: TObject);
var
  lstTmp: TStringList;
begin
  mmoTNS.Clear;
  if (Trim(mmoTNS.Text) <> '') or (not FileExists(FOracleTNSName)) then
  begin
    ShowTips('Oracle连接不存在,不能完成加载!',True);
    Exit;
  end;
  try
    lstTmp := TStringList.Create;
    lstTmp.LoadFromFile(FOracleTNSName);
    mmoTNS.Text := Trim(lstTmp.Text);
  finally
    if lstTmp <> nil then FreeAndNil(lstTmp);
  end;
end;

procedure TFormTNSEditor.pnlSaveTNSNameClick(Sender: TObject);
var
  lstTmp: TStringList;
begin
  if Trim(mmoTNS.Text) = '' then
  begin
    ShowTips('当前没有任何连接内容,不能保存!',True);
    Exit;
  end;
  try
    lstTmp := TStringList.Create;
    lstTmp.Text := mmoTNS.Text;
    lstTmp.SaveToFile(FOracleTNSName);
    ShowTips({FOracleTNSName + }'设置保存完毕!');
  finally
    if lstTmp <> nil then FreeAndNil(lstTmp);
  end;
end;

{FormatCreateTable部分}
procedure SetText(S: string);
var H: Integer;
begin
  H := IDE_GetEditorHandle;
  if H > 0 then
  begin
    SendMessage(H, em_ReplaceSel, Integer(True), Integer(PChar(S)));
  end;
end;

procedure DoFormatCreateTable(NewSQLWindow: Boolean);
var
  currentStr,dealStr,currentLine,lineComment,tmpStr,comment: string;
  tableName: string;
  strText,strResult,strLine: TStringList;
  index,count,tmpInt,alterIndex: Integer;
begin
  IDE_SetStatusMessage('');
  if IDE_GetReadOnly = True then
  begin
    IDE_SetStatusMessage('当前处于只读模式!');
    Exit;
  end;
  currentStr := IDE_GetSelectedText;
  currentStr := Trim(currentStr);
  if currentStr = '' then
  begin
    currentStr := IDE_GetText;
    currentStr := Trim(currentStr);
  end;
  if currentStr = '' then Exit;
  try
    dealStr := currentStr;
    strText := TStringList.Create;
    strResult := TStringList.Create;
    strLine := TStringList.Create;
    try
      SplitColumns(dealStr,strText,#$D#$A);
      count := strText.Count;
      if count = 0 then Exit;
      for index := 0 to count - 1 do
      begin              
        tmpStr := '';
        strLine.Clear;
        currentLine := Trim(strText[index]);
        if currentLine = '' then Continue;
        if (Copy(currentLine,1,1) = '-') then continue;
        tmpInt := Pos(' table ',LowerCase(currentLine));
        if tmpInt > 0 then
        begin
          alterIndex := Pos(' alter ',' '+LowerCase(currentLine));
          if alterIndex <= 0 then //create table
          begin
            lineComment := 'comment on table ${TABLE_NAME} is ''${TABLE_COMMENT}'';' + #13;
            tmpStr := Trim(Copy(currentLine,tmpInt + Length('table '),MaxInt));
            SplitColumns(tmpStr,strLine,'--');
            tableName := Trim(strLine.Strings[0]);
            tmpInt := strLine.Count;
            SplitColumns(tableName,strLine,' ');
            if strLine.Count > 1 then tableName := strLine.Strings[0];
            if tmpInt <= 1 then Continue;
            //存在表注释
            lineComment := StringReplace(lineComment,'${TABLE_NAME}',tableName,[rfReplaceAll]);
            tmpInt := Pos('--',currentLine) + 2;
            comment := Copy(currentLine,tmpInt,MaxInt);
            lineComment := StringReplace(lineComment,'${TABLE_COMMENT}',comment,[rfReplaceAll]);
          end
          else begin  //alter table
            tmpStr := Trim(Copy(currentLine,tmpInt + Length('table '),MaxInt));
            SplitColumns(tmpStr,strLine,' ');
            tableName := Trim(strLine.Strings[0]);
          end;
        end else
        begin
          if (Copy(currentLine,1,1) = '(') or (Copy(currentLine,1,1) = ')')
            or (Copy(currentLine,1,1) = ';')
          then
            Continue;
          lineComment := 'comment on column '+ tableName + '.${COLUMN_NAME} is ''${COLUMN_COMMENT}'';';
          tmpStr := currentLine;                      
          SplitColumns(tmpStr,strLine,'--');
          if strLine.Count <= 1 then Continue; //存在字段注释则抽取字段名和字段备注
          dealStr := strLine.Strings[0];
          tmpInt := Pos('--',currentLine) + 2;
          comment := Copy(currentLine,tmpInt,MaxInt);
          strLine.Clear;
          SplitColumns(dealStr,strLine,' ');
          if strLine.Count < 2 then Continue;
          tmpStr := strLine.Strings[0];
          lineComment := StringReplace(lineComment,'${COLUMN_NAME}',tmpStr,[rfReplaceAll]); //设置字段名
          tmpStr := comment;
        end;
        lineComment := StringReplace(lineComment,'${COLUMN_COMMENT}',tmpStr,[rfReplaceAll]);
        strResult.Add(lineComment);
      end;
      if Trim(strResult.Text) = '' then
      begin
        ShowTips('未能对表进行自动备注生成的分析');
      end
      else begin
        if NewSQLWindow then
        begin
          tmpStr := currentStr {+ #10#13 }+ #13 + strResult.Text;
          IDE_CreateWindow(1, PChar(tmpStr), False);
        end else
        begin
          tmpStr := {#10#13 + }#13 + strResult.Text;
          SetText(tmpStr);
        end;
      end;
      IDE_SetStatusMessage('备注生成完毕!');
    except
      on em: Exception do ShowTips('备注生成失败:' +#10#13+ em.Message,True);
    end;
  finally
    strText.Free;
    strResult.Free;
    strLine.Free;
  end;
end;

procedure OracleEnvInit; //创建系统环境变量
var
  Success: Boolean;
  KeyName,KeyValue: string;
  Registry: TRegistry;
const
  REG_ENV_LOCATION='\System\CurrentControlSet\Control\Session Manager\Environment';//环境变量存储在注册表中的位置
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_LOCAL_MACHINE;
    Success := Registry.OpenKey(REG_ENV_LOCATION,True);
    if Success then
    begin
      KeyName := 'LANG';
      KeyValue := 'zh_CN.GBK';
      Registry.WriteString(KeyName,KeyValue); //设置字符编码
      SetEnvironmentVariable(PChar(KeyName),PChar(KeyValue)); //更新当前进程的环境变量值
      KeyName := 'NLS_LANG';
      KeyValue := 'SIMPLIFIED CHINESE_CHINA.ZHS16GBK';
      Registry.WriteString(KeyName,KeyValue); //设置Oracle客户端字符编码
      SetEnvironmentVariable(PChar(KeyName),PChar(KeyValue)); //更新当前进程的环境变量值
      SendMessage(HWND_BROADCAST,WM_SETTINGCHANGE,0,Integer(PChar('Environment')));//发送消息通知所有上层的窗口,环境参数发生变化
    end else ShowTips('设置系统运行参数失败!');
  finally
    Registry.CloseKey;
    Registry.Free;
  end;
end;

{插件API}
function IdentifyPlugIn(ID: Integer): PChar;  cdecl;
begin
  PlugInID := ID;
  Result := 'PLSQLDeveloper扩展工具插件';
end;

procedure RegisterCallback(Index: Integer; Addr: Pointer); cdecl;
begin
  case Index of
     4 : @SYS_OracleHome := Addr;
    15 : @IDE_GetAppHandle := Addr;   
    20 : @IDE_CreateWindow := Addr;
    25 : @IDE_SetReadOnly := Addr;
    26 : @IDE_GetReadOnly := Addr;
    30 : @IDE_GetText := Addr;
    31 : @IDE_GetSelectedText := Addr;
    33 : @IDE_GetEditorHandle := Addr; 
    35 : @IDE_SetStatusMessage := Addr;
    69 : @IDE_CreatePopupItem := Addr;
  end;
end;

function CreateMenuItem(Index: Integer): PChar;  cdecl;
begin
  Result := '';
  case Index of
    1 : Result := 'WPlugins / Non-Install / OracleEnvInit';
    2 : Result := 'WPlugins / WExtend / TNSEditor';
    3 : Result := 'WPlugins / WExtend / ReadOnly';
    4 : Result := 'WPlugins / WExtend / GenComments';
    5 : Result := 'WPlugins / WExtend / GenCommentsCreate';
  end;
end;

procedure OnMenuClick(Index: Integer);  cdecl;
var
  readonly: LongBool;
begin
  try
    case Index of
      1: OracleEnvInit;
      2: DoTNSEdit(SYS_OracleHome + 'NETWORK\ADMIN\tnsnames.ora');
      3:
        begin
          readonly := IDE_GetReadOnly;
          if readonly then
          begin
           readonly := False;
           IDE_SetStatusMessage('');
          end else begin
           readonly := True;
           IDE_SetStatusMessage('当前处于只读模式!')
          end;
          IDE_SetReadOnly(readonly);
        end;
      4: DoFormatCreateTable(False);
      5: DoFormatCreateTable(True);
    end;
  except
    on em: Exception do ShowTips(em.Message,True);
  end;
end; 

procedure OnPopup(ObjectType, ObjectName: PChar); cdecl;
begin
  IDE_CreatePopupItem(PlugInID,4,'GenComments','SQLWINDOW');
  IDE_CreatePopupItem(PlugInID,5,'GenCommentsCreate','SQLWINDOW');
  IDE_CreatePopupItem(PlugInID,3,'ReadOnly','SQLWINDOW');
end;

procedure OnActivate;
begin
  if IDE_GetReadOnly = True then IDE_SetStatusMessage('当前处于只读模式!')
  else IDE_SetStatusMessage('');
end;

function About: PChar; cdecl;
begin
  Result := 'Copyright @2010- 甘一雨晨';
end;

exports
  IdentifyPlugIn,
  CreateMenuItem,
  RegisterCallback,
  OnMenuClick,
  OnPopup,
  OnActivate,
  About;

end.
