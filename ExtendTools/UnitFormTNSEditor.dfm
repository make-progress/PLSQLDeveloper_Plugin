object FormTNSEditor: TFormTNSEditor
  Left = 385
  Top = 265
  Width = 558
  Height = 366
  AlphaBlend = True
  AlphaBlendValue = 200
  BorderStyle = bsSizeToolWin
  Caption = 'TNSEditor'
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlSaveTNSName: TPanel
    Left = 0
    Top = 316
    Width = 550
    Height = 23
    Align = alBottom
    BorderStyle = bsSingle
    Caption = #20445#23384#37197#32622
    TabOrder = 0
    OnClick = pnlSaveTNSNameClick
  end
  object mmoTNS: TMemo
    Left = 0
    Top = 0
    Width = 550
    Height = 316
    Align = alClient
    BorderStyle = bsNone
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    ScrollBars = ssVertical
    TabOrder = 1
  end
end
