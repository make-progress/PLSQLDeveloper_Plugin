# PLSQLDeveloper_Plugin

#### 项目介绍
PLSQLDeveloper_Plugin是一个运行于PL/SQL Developer之下的插件，采用Delphi7开发，目前包含的功能有：
1. 设置Oracle运行需要的环境变量（应对于采用InstantClient而非安装的环境）
2. 编辑TNS文件
3. 设置SQL窗体的只读性
4. 将创建表脚本的习惯注释转换成执行注释（本编辑窗体）
5. 功能同上，只是将选定的创建内容在新的SQL窗体中转成完整的执行脚本


#### 安装教程

1. 下载项目的二进制文件或执行编译
2. 将输出文件放置于PL/SQL Developer运行目录的PlugIns目录下

#### 使用说明

本插件会在菜单栏生成一个WPlugins主菜单，其会有Non-Install和WExtend两个子菜单
1. 在Non-Install下的OracleEnvInit是为没有安装OracleClient而是使用了InstantClient的用户用来设置环境的，否则会产生中文乱码
2. WExtend下的TNSEditor时用来编辑TNS的，熟悉TNS内容的人可以直接编辑而不需要使用配置助手
3. WExtend下的ReadOnly可以将SQL编辑窗体设置成只读，以免在思考问题或其他事情时误触键盘而键入字符（同时提供右键功能）
4. WExtend下的GenComments是将表创建语句中的注释转成可执行语句（同时提供右键功能）
5. WExtend下的GenCommentsCreate功能同上，只是会将选择的创建脚本在新的SQL编辑窗中显示（同时提供右键功能）
如下脚本：
create table table_name--测试表  
(  
  id varchar2(32) not null,--guid主键  
  code varchar2(32) not null,--编码  
  name varchar2(60) not null,--名称  
  constraint pk_table_name primary key(id)  
);  
将被转换成  
create table table_name--测试表  
(  
  id varchar2(32) not null,--guid主键  
  code varchar2(32) not null,--编码  
  name varchar2(60) not null,--名称  
  constraint pk_table_name primary key(id)  
);  
comment on table table_name is '测试表';  
  
comment on column table_name.id is 'guid主键';  
comment on column table_name.code is '编码';  
comment on column table_name.name is '名称';  



#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)